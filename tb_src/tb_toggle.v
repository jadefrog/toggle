`timescale 1ns/1ps
module tb_toggle (
);

reg clk;
reg rstn;

reg din;
wire dout;

toggle i_dut(
    .clk (clk),
    .rstn (rstn),
    .din (din),
    .dout (dout)
);

initial begin
    clk = 0;
    forever #10000 clk = ~clk;
end

initial begin
    rstn = 1;
    repeat(5) @ (posedge clk);
    rstn = 0;
    repeat(5) @ (posedge clk);
    rstn = 1;
end

initial begin
    din = 1;
    repeat(20) @ (negedge clk);
    din = 0;
    @ (negedge clk);
    if (dout != 1'b1)
        $stop;
    repeat(5) @ (negedge clk);
    din = 1;
    if (dout != 1'b1)
        $stop;
    repeat(5) @ (negedge clk);
    din = 0;
    @ (negedge clk);
    if (dout != 1'b0)
        $stop;
    
    $finish;
end

endmodule //tb_toggle