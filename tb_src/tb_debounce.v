`timescale 1ns/1ps
module tb_debounce ();

reg clk;
reg rstn;

reg din;
wire dout;

debounce i_dut(
    .clk (clk),
    .rstn (rstn),
    .din (din),
    .dout (dout)
);

initial begin
    clk = 0;
    forever #10000 clk = ~clk;
end

initial begin
    rstn = 1;
    repeat(5) @ (posedge clk);
    rstn = 0;
    repeat(5) @ (posedge clk);
    rstn = 1;
end

initial begin
    din = 0;
    repeat(15) @ (negedge clk);
    din = 1;
    repeat(5) @ (negedge clk);
    if (dout != 1'b0)
        $stop;
    repeat(5) @ (negedge clk);
    din = 0;
    repeat(1) @ (negedge clk);
    if (dout != 1'b0)
        $stop;
    repeat(10) @ (negedge clk);

    din = 1;
    repeat(5) @ (negedge clk);
    if (dout != 1'b0)
        $stop;
    repeat(300) @ (negedge clk);
    if (dout != 1'b1)
        $stop;
    din = 0;
    @ (negedge clk);
    if (dout != 1'b0)
        $stop;

    $finish;

end

endmodule //tb_debounce