
module top (
    input clk,
    input rstn,

    input button,
    output led
);

reg button_sync1;
reg button_sync2;
reg rstn_sync1;
reg rstn_sync2;

always @ (posedge clk)
begin
    button_sync1 <= button;
    button_sync2 <= button_sync1;

    rstn_sync1 <= rstn;
    rstn_sync2 <= rstn_sync1;
end

wire db_button;

debounce i_debounce (
    .clk (clk),
    .rstn (rstn_sync2),

    .din (button_sync2),
    .dout (db_button)
);

toggle i_toggle (
    .clk (clk),
    .rstn (rstn_sync2),

    .din (db_button),
    .dout (led)
);

endmodule //top