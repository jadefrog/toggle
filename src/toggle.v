module toggle (
    input clk,
    input rstn,
    input din,
    output reg dout
);

reg in_dly;

always @(posedge clk or negedge rstn) begin
    if(!rstn)
        dout <= 1'b0;
    else begin
        in_dly <= din;        
        if (!din & in_dly)
            dout <= ~dout;
    end
end
    
endmodule