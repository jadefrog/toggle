module debounce (
    input clk,
    input rstn,
    input din,
    output reg dout
);

reg [7:0] count;

always @(posedge clk or negedge rstn) begin
    if(!rstn) begin
        count <= 8'h00;
        dout <= 1'b0;
    end
    else begin
        if (din & (count == 8'hFF))
            dout <= 1'b1;
        else if(din) begin
            count <= count + 8'h01;
        end
        else begin
            count <= 8'h00;
            dout <= 1'b0;
        end
    end
end

endmodule //debounce